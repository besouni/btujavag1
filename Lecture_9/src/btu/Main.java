package btu;

import java.util.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int [] m1 = new int[4];
        ArrayList a1 = new ArrayList();
        a1.add(12);
        a1.add(34.5);
        a1.add("Java");
        a1.add(true);
        a1.add(1, 9);
        a1.remove(3);
        System.out.println(a1);
        System.out.println(a1.get(0));
        LinkedList a2 = new LinkedList();
        a2.add(23);

        ArrayList <Object> a3 = new ArrayList<Object>();
        Object o = "djnffs";
        Object [] m2 = new Object[4];
        Main [] m3 = new Main[3];
        Random random = new Random();
        System.out.println(random.nextInt(10));
        System.out.println(random.nextBoolean());
        System.out.println(Math.random());
        System.out.println(a1.size());

        HashSet h1 = new HashSet();
        h1.add(3);
        h1.add(3);
        h1.add(47);
        h1.add(3);
        h1.add(23);
        System.out.println(h1);

        HashMap h2 = new HashMap();
        h2.put("Lang", a1);
        h2.put("Z", "Luka");
        System.out.println(h2);
    }
}
