package btu;

public class Student {
    String name = "Luka";
    String lastname;
    int age;
    double gpi;

    public Student() {
        System.out.println("Empty constructor is running!!");
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", age=" + age +
                ", gpi=" + gpi +
                '}';
    }

    public void printClassData() {
        System.out.println(
                "Student{" +
                    "name='" + name + '\'' +
                    ", lastname='" + lastname + '\'' +
                    ", age=" + age +
                    ", gpi=" + gpi +
                '}');
    }

    public Student(String name, String lastname) {
        this.name = name;
        this.lastname = lastname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setAge(int _age) {
        age = _age;
    }

    void checkAge(){
        if(age<18){
            System.out.println("is not adult");
        }else{
            System.out.println("is adult");
        }
    }

    void printAgeAfterYears(){
        System.out.println(age+10);
    }

    void printAgeAfterYears(int n){
        System.out.println(age+n);
    }

    void printAgeAfterYears(int n, float m){
        System.out.println(age+n-m);
    }

    void printAgeAfterYears(float n, int m){
        System.out.println(age-m+n);
    }

    void printAgeAfterYears(float n){
        System.out.println(age+n);
    }
}
