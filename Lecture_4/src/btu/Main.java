package btu;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int x = 9;
        int y = 0;
        int [] m = {3, 4, 4, 9};

        try{
            System.out.println("Try -> "+(x/y));
        }catch(ArrayIndexOutOfBoundsException  e){
            System.out.println("Catch -> "+(x/(y+1)));
        }finally {
            System.out.println("Final Block");
        }

        System.out.println("After Final Block");



        System.out.println("==============================");
        try{
            System.out.println("Before");
            System.out.println("Try -> "+(x/y));
            System.out.println("Array Try -> "+m[17]);
            System.out.println("After");
        }catch(ArrayIndexOutOfBoundsException | ArithmeticException  e){
            System.out.println("Catch -> "+(x/(y+1)));
            System.out.println("Array Catch -> "+m[m.length-1]);
            System.out.println(e.getMessage());
        }

        try{
            System.out.println("Before");
            System.out.println("Try -> "+(x/y));
            System.out.println("Array Try -> "+m[17]);
            System.out.println("After");
        }catch(Exception  e){
            System.out.println("Catch -> "+(x/(y+1)));
            System.out.println("Array Catch -> "+m[m.length-1]);
            System.out.println(e.getMessage());
        }finally {
            System.out.println("Final Block");
        }



//        String s = "Java";
//        System.out.println(s.charAt(17));
//        try {
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }
}
