package testThread1;

public class Thread1 extends Thread {

    @Override
    public void run() {
        this.setName("MyThread_1");
//        System.out.println("Thread1 - "+Thread.currentThread().getName());
        for(int i=0; i<10; i++){
            System.out.println(this.getName()+" - "+i);
        }
    }

    public void test1(){
        System.out.println("Thread1 test1 - "+this.getName());
    }
}
