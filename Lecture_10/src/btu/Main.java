package btu;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Main {

    public static void main(String[] args) {
	// write your code here

        Client c = new Client();
        Thread c_t = new Thread(c);
        Server s = new Server();
        s.start();
        c_t.start();

//        try {
//            System.out.println(InetAddress.getLocalHost());
//            System.out.println(InetAddress.getByName("localhost"));
//            System.out.println(InetAddress.getLoopbackAddress());
//            System.out.println(InetAddress.getLocalHost().getHostName());
//            System.out.println(InetAddress.getLocalHost().getHostAddress());
//        } catch (UnknownHostException e) {
//            e.printStackTrace();
//        }
    }
}
