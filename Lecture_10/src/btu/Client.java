package btu;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class Client implements Runnable {
    Socket socket;
    ObjectOutputStream objectOutputStream;
    String message;
    ObjectInputStream objectInputStream;


    @Override
    public void run(){
        try {
            while (true) {
                socket = new Socket(InetAddress.getByName("localhost"), 8888);
                message = "hello";
                objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                objectOutputStream.writeObject(message);
                objectInputStream = new ObjectInputStream(socket.getInputStream());
                System.out.println(objectInputStream.readObject());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
