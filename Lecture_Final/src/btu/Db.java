package btu;

import java.sql.*;

public class Db {
    Connection connection;
    public Db(){
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/javabtu1", "root", "");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet select(){
        ResultSet result =  null;
        String sql = "SELECT * FROM users";
        try {
            Statement statement = connection.createStatement();
            result = statement.executeQuery(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
