package btu;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;




public class HomeView implements Initializable {

    @FXML
    public VBox container;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            container.setSpacing(10);
            System.out.println("Test");
            Db db = new Db();
            ResultSet resultSet = db.select();
            while (resultSet.next()){
                Label username = new Label(resultSet.getString("username"));
                container.getChildren().add(username);
            }
        }catch (Exception e) {

        }
    }

    public void insert(){
        Stage stage = new Stage();
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("InsertView.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene scene = new Scene(parent);
        stage.setScene(scene);
        stage.show();
    }
}
