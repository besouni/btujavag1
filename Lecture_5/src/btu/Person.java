package btu;

public class Person implements PersonInterface, PersonInterface2 {

    String name;
    String lastname;
    int age;

    public Person(){

    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean CheckAge() {
        return false;
    }

    @Override
    public String getLastname() {
        return null;
    }
}
